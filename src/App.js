import React , {Fragment} from 'react';
import "./styles/styles.scss";
import Curso from './Curso';

const cursos = [

      {
        "title" : "React desde cero",
        "img" : "https://edteam-media.s3.amazonaws.com/courses/original/f7dad9a6-e060-4305-9adf-b9a9079075de.jpg",
        "price" : "50",
        "profesor" : "Alex Gomez"
      },
      {
          "title" : "Laravel desde cero",
        "img" : "https://edteam-media.s3.amazonaws.com/courses/original/4af57900-d787-4256-8eb7-d3984602e312.jpg",
        "price" : "82",
        "profesor" : "Alvaro Medina"
      },
      {
        "title" : "JavaScript desde cero",
        "img" : "https://edteam-media.s3.amazonaws.com/courses/original/87ce607a-520c-48c5-af89-6aac7cb85d6d.jpg",
        "price" : "50",
        "profesor" : "Alejandro Rodriguez"
      }    
]

const App = () =>(
  <>
      <div className="main-banner img-container l-section" id="main-banner">
      <div className="ed-grid lg-grid-6">
        <div className="lg-cols-4 lg-x-2">
          <img className="main-banner__img" src="https://ep01.epimg.net/elpais/imagenes/2019/10/30/album/1572424649_614672_1572453030_noticia_normal.jpg"/>
          <div className="main-banner__data s-center">
            <p className="t2 s-mb-0">Cursos de Edteam</p>
            <p> Subtítulo del banner</p>
            <a href="https://ed.team" className="button">Contacto</a>
          </div>
        </div>
      </div>
    </div>

<div className="ed-grid m-grid-3">
    
    {
      cursos.map( c => < Curso title={c.title} img={c.img} price={c.price} profesor={c.profesor}   /> )
    }
</div>
</>
 )


export default App;

//Reglas JSX
//1 : Todas las etiquetas deben cerrarse
//2 : Los componentes deben devolver sòlo elemento padre
//3 : Apoyarse de los fragment cuando necesite devolver dos elementos 
//4 : Fragment => <> hijos </>
//5 : Img siempre se cierra
//6 : class => pasa a ser className
//7 : for => htmlFor