import React from 'react';
import ReactDOM from 'react-dom';
import App from './App' 

const root = document.getElementById("root")
// const elemento = React.createElement(componentes,propiedades,elementos);
// const elemento = React.createElement("h1",{className:"saludo"} ,"hola mundo");

// ReactDOM.render(elemento,root);
ReactDOM.render(<App />,root);
