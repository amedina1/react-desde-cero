import React from 'react' 
import PropTypes from 'prop-types'

const mayorEdad = edad => edad >18

const Curso = ({title,img,price,profesor}) =>(
<article className="card">

<div className="img-container s-ratio-16-9 s-radius-tr s-radius-tl">
<img 
    src={img}  
    alt={title}
/>
</div>
<div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
<h3 className="center">{title}</h3>   
<div className="s-main-center">
  {profesor}
</div>

  <div className="s-main-center">
    <a className="button--ghost-alert button--tiny" href="#">{price} USD</a>
  </div>
</div>
</article>
)
   
Curso.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
    price: PropTypes.string,
    profesor: PropTypes.string 
}

Curso.defaultProps = {

  title : "No se encontro titulo",
  img : "https://image.freepik.com/vector-gratis/blanco-negro-ciudades-panorama_25-1387.jpg",
  price : "--",
  profesor : ""

}
export default Curso;   